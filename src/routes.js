import React from 'react'
import { Route, IndexRoute } from 'react-router'
import App from './containers/App'
import PostsPage from './containers/PostsPage'
import PostPage from './containers/PostPage'

Array.prototype.next = function() {
	    return this[++this.current];

};
Array.prototype.prev = function() {
	    return this[--this.current];

};
Array.prototype.current = 0;

export const CATEGORY_PREFIX = '/category/'
export const NAVIGATIONS_DATA = [
	'recommend', 
	'87',
	'80',
	'94',
	'130',
	'deep',
	'109',
	'104',
	'123'
]

export const pathPost = "/category/:path/:id"
export const pathPostEn = "category/:path/:id/:local"
export const pathPostRe = /\/category\/\d*\/(\d*)/
export const pathPostReEn = /category\/\d*\/(\d*)\/(\s*)/

export default <Route path="/" component={App}>
	<IndexRoute component={PostsPage}/>
  <Route path="/category/:path"
         component={PostsPage} />
  <Route path={pathPost} component={PostPage} />
  <Route path={pathPostEn} component={PostPage} />
</Route>

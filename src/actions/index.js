import { CALL_API, Schemas } from '../middleware/api'

export const UPDATE_SWIPER_INSTANCE = 'UPDATE_SWIPER_INSTANCE'

export const POSTS_REQUEST = 'POSTS_REQUEST'
export const POSTS_SUCCESS = 'POSTS_SUCCESS'
export const POSTS_FAILURE = 'POSTS_FAILURE'

export const initSwiper = () => (dispatch, getState) => {
	const action = {
			type: UPDATE_SWIPER_INSTANCE
		}
  return dispatch(action)
}


// Fetches posts from api
// Relies on the custom API middleware defined in ../middleware/api.js.
const fetchPosts = (category, nextPageUrl) => ({
	posts: category,
  [CALL_API]: {
    types: [ POSTS_REQUEST, POSTS_SUCCESS, POSTS_FAILURE ],
    endpoint: nextPageUrl,
    schema: Schemas.POST_ARRAY
  }
})

// Fetch posts: a list of post
// Relies on Redux Thunk middleware.
export const loadPosts = (category, nextPageUrl) => (dispatch, getState) => {
	const nextPageUrlDefault = `category/${category}`
  const {
    nextPageUrl = `/api/v2015/category/${category}`
  } = getState().pagination.pagedPosts[category] || {}
	let nextPageUrlRefine = nextPageUrl ? nextPageUrl : nextPageUrlDefault
  return dispatch(fetchPosts(category, nextPageUrlRefine))
}

// Fetch post
export const POST_REQUEST = 'POST_REQUEST'
export const POST_SUCCESS = 'POST_SUCCESS'
export const POST_FAILURE = 'POST_FAILURE'

// Fetches post: one post
// Relies on the custom API middleware defined in ../middleware/api.js.
const fetchPost = (id) => ({
	post: id,
  [CALL_API]: {
    types: [ POST_REQUEST, POST_SUCCESS, POST_FAILURE ],
    endpoint: `/api/v2015/post/${id}`,
    schema: Schemas.POST
  }
})

// Fetch post: one post
// Relies on Redux Thunk middleware.
export const loadPost = (id) => (dispatch, getState) => {
  return dispatch(fetchPost(id))
}

// Fetch related post
export const POST_RELATED_REQUEST = 'POST_RELATED_REQUEST'
export const POST_RELATED_SUCCESS = 'POST_RELATED_SUCCESS'
export const POST_RELATED_FAILURE = 'POST_RELATED_FAILURE'

// Fetches post: one post
// Relies on the custom API middleware defined in ../middleware/api.js.
const fetchRelatedPost = (id) => ({
	related_posts: id,
  [CALL_API]: {
    types: [ POST_RELATED_REQUEST, POST_RELATED_SUCCESS, POST_RELATED_FAILURE ],
    endpoint: `/api/v2015/related/${id}`,
    schema: Schemas.POST_ARRAY
  }
})

// Fetch post: one post
// Relies on Redux Thunk middleware.
export const loadRelatedPost = (id) => (dispatch, getState) => {
	const relatedData = getState().related.related
	// if related articles not load, then load
	if(typeof relatedData[id] === 'undefined') {
		return dispatch(fetchRelatedPost(id))
	}
}

export const RESET_ERROR_MESSAGE = 'RESET_ERROR_MESSAGE'

// Resets the currently visible error message.
export const resetErrorMessage = () => ({
    type: RESET_ERROR_MESSAGE
})


export const UPDATE_NAV_BTN = 'UPDATE_NAV_BTN'
export const NAV_CLASS_DEFAULT = 'burger opaque'
export const NAV_CLASS_ON = 'burger opaque transform'
export const NAV_MODAL_CLASS_DEFAULT = 'sy-downward nav-hide'
export const NAV_MODAL_CLASS_ON = 'sy-downward nav-on'
/**
 * 当hide为true的时候强制关闭
 */
export const dispatchNavClick = (currentClass, hide = false) => (dispatch, getState) => {
	const state = getState();
	const action = {
			type: UPDATE_NAV_BTN,
			currentClass: currentClass
		}
  return dispatch(action)
}

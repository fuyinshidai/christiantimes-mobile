import React from 'react'
import { render } from 'react-dom'
import { browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'
import Root from './containers/Root'
import configureStore from './store/configureStore'

const store = configureStore()
browserHistory.listen(function(ev) {
  // 将原来的新闻页面重定向
  var news = ev.pathname.match(/news\/(\d*)/)
  if(news) {
	  var newsId = news[1];
	  window.location.replace("/category/89/"+newsId);
  }
	const regexList = [/section/, /gall/, /video/, /column/, /topic/];
	const isMatch = regexList.some(rx => rx.test(ev.pathname));
  if(isMatch) {
	  window.location.replace("/");
  }
});
const history = syncHistoryWithStore(browserHistory, store)



render(
  <Root store={store} history={history} />,
  document.getElementById('root')
)

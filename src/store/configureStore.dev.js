import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import createLogger from 'redux-logger'
import api from '../middleware/api'
import rootReducer from '../reducers'
import DevTools from '../containers/DevTools'

import observe from '../reducers/reduxStoreObserver.js';
import { loadRelatedPost } from '../actions/'
import { pathPostRe } from '../routes'

const configureStore = preloadedState => {
  const store = createStore(
    rootReducer,
    preloadedState,
    compose(
      applyMiddleware(thunk, api, createLogger()),
      DevTools.instrument()
    )
  )

	observe(store,
			//if THIS changes, we the CALLBACK will be called
			state => state.routing.locationBeforeTransitions.pathname, 
			(store, previousValue, currentValue) => {
				//console.log('Some property changed from ', previousValue, 'to', currentValue);
				// Load the related posts
				const inPostPage = currentValue.match(pathPostRe)
				if(inPostPage) {
					store.dispatch(loadRelatedPost(inPostPage[1]))
				}
			}
	)

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers').default
      store.replaceReducer(nextRootReducer)
    })
  }

  return store
}

export default configureStore

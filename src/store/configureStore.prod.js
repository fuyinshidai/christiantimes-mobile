import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import api from '../middleware/api'
import rootReducer from '../reducers'

import observe from '../reducers/reduxStoreObserver.js';
import { loadRelatedPost } from '../actions/'
import { pathPostRe } from '../routes'

const configureStore = preloadedState => {
	const store = createStore(
		rootReducer,
		preloadedState,
		applyMiddleware(thunk, api)
	)

	observe(store,
			//if THIS changes, we the CALLBACK will be called
			state => state.routing.locationBeforeTransitions.pathname, 
			(store, previousValue, currentValue) => {
				//console.log('Some property changed from ', previousValue, 'to', currentValue);
				// Load the related posts
				const inPostPage = currentValue.match(pathPostRe)
				if(inPostPage) {
					store.dispatch(loadRelatedPost(inPostPage[1]))
				}
			}
	)

	return store
}


export default configureStore

/*
 * 新闻列表的新闻单个显示
 * 显示新闻的标题、简介、作者、时间等信息
 */
import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'

export default class PostTeaser extends Component {

  static defaultProps = {
		post: PropTypes.object.isRequired,
  }


  render() {
    const {
			post, index
    } = this.props
		const url = "/category/"+ post.category + "/" + post.id

		let imgs, content
		if(index === 0) {
			imgs = <div className="news-imga clearfix">
	            	<Link to={url}><img src={post.imagesCoverBig} alt={post.title} /></Link>
	            </div>

			content = <div>
						<h1 className="news-h1"><Link to={url}>{post.title}</Link></h1>
						<div className="time clearfix">
							<span>{post.created}</span>
						</div>
					</div>
					return <article className="clearfix">
	    	<div className="news-list container clearfix">
					{imgs}
					{content}
	    	</div> 
	    </article>
		} else if(post.imagesCount === 1) {
			imgs = <div className="news-imgb fr">
							{post.imagesThumbs.map((img) => <Link key={img} to={url}><img src={img} alt={post.title} /></Link>)}
						</div>
			content = <div className="news-listb fl">
						<h1 className="news-h1"><Link to={url}>{post.title}</Link></h1>
						<div className="time clearfix">
							<span>{post.created}</span>
						</div>
					</div>
		} else if(post.imagesCount > 1) {
			imgs = <div className="news-imgc clearfix">
							{post.imagesThumbs.map((img) => <Link key={img} to={url}><img src={img} alt={post.title} /></Link>)}
						</div>
			content = <div>
								<h1 className="news-h1"><Link to={url}>{post.title}</Link></h1>
								<div className="time clearfix">
									<span>{post.created}</span>
								</div>
							</div>
		}
    return (
			<article className="clearfix">
	    	<div className="news-list container clearfix">
					{content}
					{imgs}
	    	</div> 
	    </article>
    )
  }
}

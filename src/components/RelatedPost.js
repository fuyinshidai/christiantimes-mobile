import React, { PropTypes } from 'react'

const RelatedPost = ({ posts, renderItem }) => {
	console.log('posts.length > 1', posts) 
	console.log('typeof posts', typeof posts) 
	if(Object.keys(posts).length === 0) {
		return <div />
	}
	if(posts) {
		return (
			<div>
				<br/>
				<div className="com-h1 red-h clearfix">
					<i></i>
					<h1>精彩推荐</h1>
				</div>
				{posts.map(renderItem)}
			</div>
		)
	}
}

RelatedPost.propTypes = {
}

export default RelatedPost

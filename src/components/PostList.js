import React, { Component, PropTypes } from 'react'

import Spin from './Spin'


export default class PostList extends Component {
  static propTypes = {
		items: PropTypes.array.isRequired,
    pageCount: PropTypes.number,
    isFetching: PropTypes.bool.isRequired,
    onLoadMoreClick: PropTypes.func.isRequired,
    renderItem: PropTypes.func.isRequired,
    nextPageUrl: PropTypes.string
  }

  static defaultProps = {
    isFetching: true
  }

  renderLoadMore() {
    const { isFetching, onLoadMoreClick } = this.props
    return <div className="loading" style={{ fontSize: '150%' }}
							onClick={onLoadMoreClick}
							disabled={isFetching}>
					<a>{isFetching ? <Spin /> : '加载更多'}</a>
				</div>
  }

  render() {
    const {
      items, renderItem, pageCount, isFetching, nextPageUrl
    } = this.props

    const isEmpty = items.length === 0
    const isLastPage = !nextPageUrl
    if (isEmpty && isFetching) {
      return <div className="loading">
				<Spin />
				</div>
    }

    return (
      <div>
        {items.map(renderItem)}
        {pageCount > 0 && !isLastPage && this.renderLoadMore()}
      </div>
    )
  }
}

import React, { PropTypes } from 'react'

import $ from 'jquery'
import praise from '../assets/img/dashang-alipay.jpg'

import ad_src from '../assets/img/nasale.jpg'
const ad_url = 'https://h5.m.taobao.com/awp/core/detail.htm?id=576816350694'
const ad_alt = '塔木德故事绘本'

function createMarkup(content) {
	  return {__html: content};
}

const handlePraise = () => {
		$(".admireCode").toggle()
}

const PostDetail = ({ post, local }) => {

	const author = post.author ? '作者：' + post.author : ''
	const from = post.from ? '来源：' + post.from : ''

	let imgs
	if(post && post.imagesDetails) {
		imgs = <div className="news-imga clearfix">
						{post.imagesDetails.map((img) => <img src={img} alt={post.title} />)}
					</div>
	}

	const styles= {
		thumb: {
			'margin': '0 auto',
			'display': 'none'
		}
	}

	const dashang = post.donate ? <div className="admire clearfix" >
					<div className="admireBtn">								
						<input type="button" name="codeBtn" className="codeBtn" id="codeBtn" value="打赏" onClick={handlePraise} />
						<span>如有共鸣和感动，欢迎赞赏支持，激励更多原创！</span>
						<div className="admireCode" >
							<i></i>
							<img alt="" src={praise} />
						</div>
					</div>								
				</div> : ''
	const title = local === 'en' ? post.titleEn : post.title
	const content = local === 'en' ? post.contentEn : post.content
	const ad = <a href={ad_url}>
                                            <img src={ad_src} alt={ad_alt}/>
                                            </a>

  return (
		<div className="container clearfix">
			<div id='wx_pic' style={styles.thumb}>
			</div>
			<div className="headline clearfix">
				<h1>{title}</h1>
				<div className="data clearfix">
					<span>{author}</span>
					<span>{from}</span>
					<span>{post.created}</span>
				</div>
			</div>
			<div className="article clearfix">
				{imgs}
				<div dangerouslySetInnerHTML={createMarkup(content)}></div>
                                            {ad}

				{dashang}
			</div>
		</div>
  )
}

PostDetail.propTypes = {
  post: PropTypes.shape({
  }).isRequired,
}

export default PostDetail

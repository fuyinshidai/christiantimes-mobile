import * as ActionTypes from '../actions'
import merge from 'lodash/merge'
import paginate from './paginate'
import relatedPosts from './related'
import { routerReducer as routing } from 'react-router-redux'
import { combineReducers } from 'redux'
import Swiper from 'swiper'

const swiper = () => {
	return new Swiper('.swiper-container', {
				slidesPerView:'6',
				freeMode:true
		});
}

// swiper the menu navigation
const swip = (state = { swiper, currentSwiper: null }, action) => {
	if(action.type === ActionTypes.UPDATE_SWIPER_INSTANCE ) {
		return {
          ...state,
          currentSwiper: swiper()
        }
	}
	return state
}

// Updates an entity cache in response to any action with response.entities.
const entities = (state = { posts: {} }, action) => {
  if (action.response && action.response.entities) {
    return merge({}, state, action.response.entities)
  }

  return state
}

const navBtnClass = (state = { navBtnClass: ActionTypes.NAV_CLASS_DEFAULT, navModalClass: ActionTypes.NAV_MODAL_CLASS_DEFAULT}, action) => {
	if(action.type === ActionTypes.UPDATE_NAV_BTN ) {
		let newClass = state.navBtnClass === ActionTypes.NAV_CLASS_DEFAULT ? ActionTypes.NAV_CLASS_ON : ActionTypes.NAV_CLASS_DEFAULT
		console.log('state.navModalClass', state.navModalClass)
		console.log('ActionTypes.NAV_MODAL_CLASS_DEFAULT', ActionTypes.NAV_MODAL_CLASS_DEFAULT)
		console.log('ActionTypes.NAV_MODAL_CLASS_ON', ActionTypes.NAV_MODAL_CLASS_ON)
		let newModalClass = state.navModalClass === ActionTypes.NAV_MODAL_CLASS_DEFAULT ? ActionTypes.NAV_MODAL_CLASS_ON : ActionTypes.NAV_MODAL_CLASS_DEFAULT
		return {
          ...state,
          navBtnClass: newClass,
					navModalClass: newModalClass
        }
	}
  return state
}

// Updates error message to notify about the failed fetches.
const errorMessage = (state = null, action) => {
  const { type, error } = action

  if (type === ActionTypes.RESET_ERROR_MESSAGE) {
    return null
  } else if (error) {
    return action.error
  }

  return state
}

// Updates the pagination data for different actions.
const pagination = combineReducers({
  pagedPosts: paginate({
    mapActionToKey: action => action.posts,
    types: [
      ActionTypes.POSTS_REQUEST,
      ActionTypes.POSTS_SUCCESS,
      ActionTypes.POSTS_FAILURE
    ]
  }),
})

// Updates the related posts
const related = combineReducers({
  related: relatedPosts({
    mapActionToKey: action => action.related_posts,
    types: [
      ActionTypes.POST_RELATED_REQUEST,
      ActionTypes.POST_RELATED_SUCCESS,
      ActionTypes.POST_RELATED_FAILURE
    ]
  }),
})

const rootReducer = combineReducers({
	navBtnClass,
	swip,
  entities,
  pagination,
	related,
  errorMessage,
  routing
})

export default rootReducer

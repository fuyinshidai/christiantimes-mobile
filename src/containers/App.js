import React, { Component, PropTypes } from 'react'
import {Link, IndexLink} from 'react-router'
import { connect } from 'react-redux'
import { resetErrorMessage, initSwiper, dispatchNavClick, NAV_CLASS_DEFAULT } from '../actions'
import DocumentTitle from 'react-document-title'

import '../assets/css/style.css'
import imgLogo from '../assets/img/newlogo.png'
import gotoTopIcon from '../assets/img/index_26.png'

import 'swiper/dist/css/swiper.min.css'

import $ from 'jquery'


class App extends Component {
  static propTypes = {
    // Injected by React Redux
    errorMessage: PropTypes.string,
    resetErrorMessage: PropTypes.func.isRequired,
		swiper: PropTypes.func.isRequired,
		dispatchNavClick: PropTypes.func.isRequired,
    children: PropTypes.node
  }

  handleDismissClick = e => {
    this.props.resetErrorMessage()
    e.preventDefault()
  }

	handleLeftSwipe = e => {
	  console.log(e);
	}

  componentWillMount() {
  }

  componentDidMount() {
		this.props.initSwiper()
		$(window).scroll(function(){
			var scrollTop = $(window).scrollTop();
			if(scrollTop > 200){
				$(".topBtn").show(400);
			}else{
				$(".topBtn").hide(400);		
			}	
		})
		$(".topBtn").click(function(){
			$("html,body").animate({"scrollTop":"0px"},500);
			return false;
		})

		// 百度统计
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?5c4d70eaa36ce75ab8aeedb5893413fa";
			var s = document.getElementsByTagName("script")[0]; 
			s.parentNode.insertBefore(hm, s);
		})();
  }

  componentWillReceiveProps(nextProps) {
  }

	handleUpdateNavigation(transformClass) {
		this.props.dispatchNavClick(transformClass)
	}

  renderErrorMessage() {
    const { errorMessage } = this.props
    if (!errorMessage) {
      return null
    }

    return (
      <p style={{ backgroundColor: '#e99', padding: 10 }}>
        <b>{errorMessage}</b>
        {' '}
        (<a href="#"
            onClick={this.handleDismissClick}>
          Dismiss
        </a>)
      </p>
    )
  }

  render() {
    const { children } = this.props
		const title = '基督时报'
		const transformClass = this.props.navBtnClass
		const navModalClass = this.props.navModalClass
    return (
		<DocumentTitle title={title}>
      <div>
				<div id="header">
					<header className="clearfix">
						<div className="logo"><Link to="/"><img src={imgLogo} alt="logo" /></Link></div>
						<div className="burger-p" onClick={() => this.handleUpdateNavigation(transformClass)}>
							<div className={ transformClass }><div></div><div ></div><div></div></div>
						</div>

						{/* 显示弹出的导航 */}
						<div className={navModalClass} >
							<div className="scrolly">
								<div className="sections-panel">
									<ul className="sections-list" >
										<li><IndexLink to="/" ref="nav1" onClick={() => this.handleUpdateNavigation(transformClass)} activeClassName="select-on">首页</IndexLink></li>
										<li><Link to="/category/87" onClick={() => this.handleUpdateNavigation(transformClass)} activeClassName="select-on">教会</Link></li>
										<li><Link to="/category/80" onClick={() => this.handleUpdateNavigation(transformClass)} activeClassName="select-on">国际</Link></li>
										<li><Link to="/category/94" activeClassName="select-on" onClick={() => this.handleUpdateNavigation(transformClass)}>社会</Link></li>
										<li><Link to="/category/130" activeClassName="select-on" onClick={() => this.handleUpdateNavigation(transformClass)}>生活</Link></li>
										<li><Link to="/category/deep" activeClassName="select-on" onClick={() => this.handleUpdateNavigation(transformClass)}>深读</Link></li>
										<li><Link to="/category/109" activeClassName="select-on" onClick={() => this.handleUpdateNavigation(transformClass)}>文化</Link></li>
										<li><Link to="/category/104" activeClassName="select-on" onClick={() => this.handleUpdateNavigation(transformClass)}>家庭</Link></li>
										<li><Link to="/category/123" activeClassName="select-on" onClick={() => this.handleUpdateNavigation(transformClass)}>商业</Link></li>
									</ul>
								</div>
							</div>
						</div>

						<nav className="clearfix">
							<div className="container clearfix">
								<div className="search clearfix">
									<div className="searInut fl">
										<input type="text" className="searText" value="" placeholder="基督教" />
										<input type="submit" className="searSub" value="" />
									</div>
									<div className="cancel fr">取消</div>
								</div>
									 <div className="swiper-container clearfix">
											<div className="swiper-wrapper clearfix">
													<div className="swiper-slide"><IndexLink to="/" ref="nav1" activeClassName="select-on">首页</IndexLink></div>
													<div className="swiper-slide"><Link to="/category/87" activeClassName="select-on">教会</Link></div>
													<div className="swiper-slide"><Link to="/category/80" activeClassName="select-on">国际</Link></div>
													<div className="swiper-slide"><Link to="/category/94" activeClassName="select-on">社会</Link></div>
													<div className="swiper-slide"><Link to="/category/130" activeClassName="select-on">生活</Link></div>
													<div className="swiper-slide"><Link to="/category/deep" activeClassName="select-on">深读</Link></div>
													<div className="swiper-slide"><Link to="/category/109" activeClassName="select-on">文化</Link></div>
													<div className="swiper-slide"><Link to="/category/104" activeClassName="select-on">家庭</Link></div>
													<div className="swiper-slide"><Link to="/category/123" activeClassName="select-on">商业</Link></div>
											</div>
									</div>
							</div>
						</nav>
					</header>
				</div>
				<div id="content">
					{this.renderErrorMessage()}
					{children}
					<div className="topBtn" style={{display: 'none'}}>
						<img src={gotoTopIcon} alt="" />
					</div>
				</div>
      </div>
		</DocumentTitle>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
	state,
  errorMessage: state.errorMessage,
	swiper: state.swip.swiper,
	navBtnClass: state.navBtnClass.navBtnClass,
	navModalClass: state.navBtnClass.navModalClass
})

export default connect(mapStateToProps, {
  resetErrorMessage,
	initSwiper,
	dispatchNavClick
})(App)

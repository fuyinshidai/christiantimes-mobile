/*
 * 显示文章详细页面
 */
import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { loadPost, loadRelatedPost } from '../actions'
import PostDetail from '../components/PostDetail'
import RelatedPost from '../components/RelatedPost'
import DocumentTitle from 'react-document-title'
import { API_HIT } from '../middleware/api'

import $ from 'jquery'

import PostTeaser from '../components/PostTeaser'

class PostPage extends Component {
  static propTypes = {
		post: PropTypes.object.isRequired,
		id: PropTypes.string.isRequired,
		loadPost: PropTypes.func.isRequired,
		related: PropTypes.array.isRequired
  }

  hit(id) {
	$.post(API_HIT, { id })
  }

  componentWillMount() {
		//alert('componentWillMount')
		const { post } = this.props
		// 当新闻不存在的时候从服务器载入新闻
		// 检查post是否存在
		const { id, loadPost, local} = this.props
		this.hit(id)
		if(Object.keys(post).length === 0 && post.constructor === Object) {
			loadPost(id)
		}
  }

  componentWillReceiveProps(nextProps) {
		//alert('next props')
		const { post } = nextProps
    if (nextProps.id !== this.props.id) {
			this.hit(nextProps.id)
			if(Object.keys(post).length === 0 && post.constructor === Object) {
				this.props.loadPost(nextProps.id)
			}
    }

		// 百度统计
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?5c4d70eaa36ce75ab8aeedb5893413fa";
			var s = document.getElementsByTagName("script")[0]; 
			s.parentNode.insertBefore(hm, s);
		})();
  }

  renderPostTeaser(post) {
    return (
      <PostTeaser
				post={post}
				key={post.id}
      />
    )
  }


  render() {
    const { post, related, local } = this.props
		$("html, body").animate({"scrollTop":"0px"},500);
		const title = post.title + ' - 基督时报'
    return (
		<DocumentTitle title={title}>
      <div ref="detail">
				<PostDetail
					post={ post }
					local={local}
				/>
				<RelatedPost
					posts={ related }
					renderItem={this.renderPostTeaser}
				/>
      </div>
		</DocumentTitle>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  const id = ownProps.params.id
  const local = ownProps.params.local
	const post = state.entities.posts[id] || {}
	const related = state.related.related[id].related.map(id => state.entities.posts[id]) || {}
  return {
		id,
		post,
		state,
		local,
		related
  }
}

export default connect(mapStateToProps, {
	loadPost
})(PostPage)

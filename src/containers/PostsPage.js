import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { loadPosts } from '../actions'
import PostList from '../components/PostList'
import PostTeaser from '../components/PostTeaser'
import { browserHistory } from 'react-router'

import { NAVIGATIONS_DATA, CATEGORY_PREFIX } from '../routes'

import ReactSwipe from 'react-swipe';
import Swipeable from 'react-swipeable';

class PostsPage extends Component {
  static propTypes = {
    path: PropTypes.string.isRequired,
		postsList: PropTypes.array.isRequired,
		postsPagination: PropTypes.object.isRequired,
		currentSwiper: PropTypes.object, loadPosts: PropTypes.func.isRequired
  }

  componentWillMount() {
		if(this.props.postsList.length === 0) {
			this.props.loadPosts(this.props.path)
		}
  }

  componentDidMount() {
  }

  componentWillReceiveProps(nextProps) {
    const { postsList} = nextProps
		// 如果更新了url并且下一个页面的内容为空则载入新闻
		if (nextProps.path !== this.props.path) {
			if(postsList.length === 0) {
				this.props.loadPosts(nextProps.path)
			}
			//this.doMenuSwip()
		}
		
		// 百度统计
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?5c4d70eaa36ce75ab8aeedb5893413fa";
			var s = document.getElementsByTagName("script")[0]; 
			s.parentNode.insertBefore(hm, s);
		})();
  }

  handleLoadMoreClick = () => {
    this.props.loadPosts(this.props.path, true)
  }

	swipedRight = () => {
		this.setNavIndex()
		const nextCategory = NAVIGATIONS_DATA.prev()
		this.gotoNextCate(nextCategory)
		this.props.currentSwiper.slidePrev()
	}

	swipedLeft = () => {
		this.setNavIndex()
		const nextCategory = NAVIGATIONS_DATA.next()
		this.gotoNextCate(nextCategory)
		this.props.currentSwiper.slideNext()
	}

	setNavIndex = () => {
    const { path } = this.props
		const indexE = NAVIGATIONS_DATA.findIndex(e => e === path);
		NAVIGATIONS_DATA.current = indexE
	}

	gotoNextCate = (nextCategory) => {
		if(typeof nextCategory !== 'undefined') {
			if(nextCategory === 'recommend') {
				browserHistory.push('/');
			} else {
				browserHistory.push(CATEGORY_PREFIX + nextCategory);
			}
		}
	}


	swipedDown = () => {
		console.log('swip down')
	}

	handleSwipe = (direction) => {
			//browserHistory.push('/');
	}

	doMenuSwip() {
	//this.props.currentSwiper.slidePre()
	}


  renderPostTeaser(post, index) {
    return (
      <PostTeaser
				index={index}
				post={post}
				key={post.id}
      />
    )
  }

  render() {
    const { path, postsList, postsPagination} = this.props

    return (
			<ReactSwipe className="carousel" swipeOptions={{continuous: false}}>
				<Swipeable
				ref="nav"
					onSwipedRight={this.swipedRight}
					onSwipedLeft={this.swipedLeft}
					onSwipedDown={this.swipedDown}
					flickThreshold={1}
					delta={100}
					>
				<PostList
					items={postsList}
					renderItem={this.renderPostTeaser}
					pageCount={1}
					path={path}
					loadingLabel='载入中...'
					onLoadMoreClick={this.handleLoadMoreClick}
					{...postsPagination}
				/>
					</Swipeable>
			</ReactSwipe>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  const path = (ownProps.params.path && ownProps.params.path.toLowerCase()) || 'recommend'

  const {
    pagination: { pagedPosts },
    entities: { posts },
  } = state
	
  const postsPagination = pagedPosts[path] || { ids: [] }
  const postsList = postsPagination.ids.map(id => posts[id])

  return {
		path,
		postsPagination,
		currentSwiper: state.swip.currentSwiper,
    postsList
  }
}

export default connect(mapStateToProps, {
	loadPosts
})(PostsPage)

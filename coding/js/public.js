(function ($) {
  // Use jQuery with the shortcut:
  //console.log($.browser);
$(function(){
	$(".sear-btn").click(function(){
		$(".swiper-container").hide();
		$(".search").fadeIn(600);
		$(".sear-btn").fadeOut(300);
	})
	$(".cancel").click(function(){
		$(".swiper-container").fadeIn(600);
		$(".search").hide();
		$(".sear-btn").fadeIn(300);
	})
	$(".topBtn").click(function(){
		$("html,body").animate({"scrollTop":"0px"},500);
		return false;
	})
	$(".disInput-le textarea").focus(function(){	
		location.href="login.html";
	})
	$(".disInput textarea").click(function(event){
		$(".sent").slideDown(200);
		$(".topBtn").animate({"bottom":"90px"},400);
		event.stopPropagation();
	})
	$("body").click(function(){
		$(".sent").slideUp(200);
		$(".topBtn").animate({"bottom":"66px"},400);
	})
})
//回到顶部
$(window).scroll(function(){
	var scrollTop = $(window).scrollTop();
	if(scrollTop > 200){
		$(".topBtn").show(400);
	}else{
		$(".topBtn").hide(400);		
	}	
})
//点赞功能
$(document).ready(function(e) {
    $('.zhan').one("click",
    function() {
        var left = parseInt($(this).offset().left) + 10,
        top = parseInt($(this).offset().top) - 10,
        obj = $(this);
        $(this).append('<div id="zhan"><b>+1<\/b></\div>');
        $('#zhan').css({
            'position': 'absolute',
            'z-index': '1',
            'color': '#C30',
            'left': left + 'px',
            'top': top + 'px'
        }).animate({
            top: top - 10,
            left: left + 10
        },
        'slow',
        function() {
            $(this).fadeIn('fast').remove();
            var Num = parseInt(obj.find('span').text());
            Num++;
            obj.find('span').text(Num);
        });
        $(this).children("img").attr("src", "img/index_39.png");
        $("this").removeClass("zhan");
        return false;
    });
});
//登录页
$(function(){
	setInterval(function(){
		$(".login ul li:nth-child(1)").animate({"padding-left":"60px"},300,function(){
			$(".login ul li:nth-child(1)").animate({"padding-left":"0px"},300);
			$(".login ul li:nth-child(2)").animate({"padding-left":"65px"},320,function(){
				$(".login ul li:nth-child(2)").animate({"padding-left":"0px"},320);
				$(".login ul li:nth-child(3)").animate({"padding-left":"70px"},400,function(){
					$(".login ul li:nth-child(3)").animate({"padding-left":"0px"},400);
					$(".login ul li:nth-child(4)").animate({"padding-left":"90px"},450,function(){
						$(".login ul li:nth-child(4)").animate({"padding-left":"0px"},450);
						});
					});
				});
			});
		},5000)
	})
// Here we immediately call the function with jQuery as the parameter.
}(jQuery));
